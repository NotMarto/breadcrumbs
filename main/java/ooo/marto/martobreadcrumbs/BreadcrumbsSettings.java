package ooo.marto.martobreadcrumbs;

import net.minecraftforge.common.config.Configuration;

public class BreadcrumbsSettings {

	
	public static boolean TNT_ENABLED = true, SAND_ENABLED = true;
	
	public static float THICKNESS = 3.0f, TNT_RED = 1.0f, TNT_GREEN = 0f, TNT_BLUE = 0f, SAND_RED = 0.2f, SAND_GREEN = 0.6f, SAND_BLUE = 1f;
	
	public static float TIME_TO_LIVE = 20;
	
}
