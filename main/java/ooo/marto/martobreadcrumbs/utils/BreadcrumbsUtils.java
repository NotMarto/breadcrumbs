package ooo.marto.martobreadcrumbs.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.vecmath.Vector3d;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityFallingBlock;
import net.minecraft.entity.item.EntityTNTPrimed;
import ooo.marto.martobreadcrumbs.BreadcrumbsSettings;

public class BreadcrumbsUtils {

	private ArrayList<CrumbTracker> currentCrumbTrackers = new ArrayList<>();

	public void addCrumbTacker(CrumbTracker crumbTracker) {
		currentCrumbTrackers.add(crumbTracker);
	}

	public void updateTick() {
		killOldBreadcrumbTrackers();
		
		List<Entity> loadedEntities = Minecraft.getMinecraft().theWorld.loadedEntityList, importantLoadedEntities = new ArrayList<>();

		for (Entity entity : loadedEntities) {
			if (!(entity instanceof EntityTNTPrimed || entity instanceof EntityFallingBlock))
				continue;

			importantLoadedEntities.add(entity);
		}

		for (Entity importantEntity : importantLoadedEntities) {
			Vector3d location = new Vector3d(importantEntity.posX, importantEntity.posY, importantEntity.posZ);

			for (CrumbTracker crumbTracker : currentCrumbTrackers) {
				if (importantEntity.getEntityId() == crumbTracker.getEntityId()) {
					crumbTracker.getLocationHistory().add(location);
					break;
				}
			}

			currentCrumbTrackers.add(new CrumbTracker(importantEntity.getEntityId(), location,
					importantEntity instanceof EntityTNTPrimed));
		}
	}

	private void killOldBreadcrumbTrackers()
	{
		Iterator<CrumbTracker> crumbTrackerIterator = currentCrumbTrackers.iterator();
		
		while(crumbTrackerIterator.hasNext())
		{
			if(System.currentTimeMillis() - crumbTrackerIterator.next().getBirthTime() > BreadcrumbsSettings.TIME_TO_LIVE * 1000)
				crumbTrackerIterator.remove();
		}
	}
	
	public void renderBreadcrumbs(float partialTicks) {

		EntityPlayerSP player = Minecraft.getMinecraft().thePlayer;

		double partialTickPositionX = player.prevPosX + (player.posX - player.prevPosX) * partialTicks;
		double partialTickPositionY = player.prevPosY + (player.posY - player.prevPosY) * partialTicks;
		double partialTickPositionZ = player.prevPosZ + (player.posZ - player.prevPosZ) * partialTicks;

		for (CrumbTracker crumbTracker : currentCrumbTrackers) {
			
			if((crumbTracker.isTNT() && !BreadcrumbsSettings.TNT_ENABLED) || (!crumbTracker.isTNT() && !BreadcrumbsSettings.SAND_ENABLED))
				return;
			
			GL11.glPushMatrix();
			GL11.glDisable(GL11.GL_LIGHTING);
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			GL11.glDisable(GL11.GL_DEPTH_TEST);

			GL11.glLineWidth(BreadcrumbsSettings.THICKNESS);
			GL11.glTranslated(-partialTickPositionX, -partialTickPositionY, -partialTickPositionZ);

			GL11.glEnable(GL11.GL_LINE_SMOOTH);
			GL11.glHint(GL11.GL_LINE_SMOOTH_HINT, GL11.GL_NICEST);
			
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

			if (crumbTracker.isTNT())
				GL11.glColor4d(BreadcrumbsSettings.TNT_RED, BreadcrumbsSettings.TNT_GREEN, BreadcrumbsSettings.TNT_BLUE, (crumbTracker.getLifeLeft() > 1 ? 1.0 : crumbTracker.getLifeLeft() / 1));
			else
				GL11.glColor4d(BreadcrumbsSettings.SAND_RED, BreadcrumbsSettings.SAND_GREEN, BreadcrumbsSettings.SAND_BLUE, (crumbTracker.getLifeLeft() > 1 ? 1.0 : crumbTracker.getLifeLeft() / 1));

			GL11.glBegin(GL11.GL_LINE_STRIP);
			
			for(Vector3d location : crumbTracker.getLocationHistory())
			{
				GL11.glVertex3d(location.x, location.y, location.z);
			}
			
			GL11.glEnd();
			
			GL11.glDisable(GL11.GL_BLEND);
			
			GL11.glEnable(GL11.GL_LIGHTING);
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			GL11.glEnable(GL11.GL_DEPTH_TEST);
			
			GL11.glPopMatrix();
		}

	}

}
