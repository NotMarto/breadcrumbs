package ooo.marto.martobreadcrumbs.utils;

import java.util.ArrayList;

import javax.vecmath.Vector3d;

import ooo.marto.martobreadcrumbs.Breadcrumbs;
import ooo.marto.martobreadcrumbs.BreadcrumbsSettings;

public class CrumbTracker {

	private int entityId;
	
	private ArrayList<Vector3d> locationHistory = new ArrayList<>();
	
	private boolean tnt;
	
	private long birthTime;
	
	public CrumbTracker(int entityId, Vector3d initialLocation, boolean tnt)
	{
		this.entityId = entityId;
		locationHistory.add(initialLocation);
		this.tnt = tnt;
		this.birthTime = System.currentTimeMillis();
		
		Breadcrumbs.getModInstance().getBreadcrumbsUtils().addCrumbTacker(this);
	}
	
	public int getEntityId()
	{
		return entityId;
	}
	
	public ArrayList<Vector3d> getLocationHistory()
	{
		return locationHistory;
	}
	
	public boolean isTNT()
	{
		return tnt;
	}
	
	public long getBirthTime()
	{
		return birthTime;
	}
	
	public float getLifeLeft()
	{
		return (BreadcrumbsSettings.TIME_TO_LIVE * 1000 - (System.currentTimeMillis() - birthTime)) / 1000;
	}
	
}
