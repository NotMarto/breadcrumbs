package ooo.marto.martobreadcrumbs.listener;

import net.minecraft.client.Minecraft;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import ooo.marto.martobreadcrumbs.Breadcrumbs;
import ooo.marto.martobreadcrumbs.utils.BreadcrumbsUtils;

public class BreadcrumbsListener {

	private BreadcrumbsUtils breadcrumbUtils;
	
	public BreadcrumbsListener()
	{
		breadcrumbUtils = Breadcrumbs.getModInstance().getBreadcrumbsUtils();
	}
	
	@SubscribeEvent
	public void onTick(TickEvent.ClientTickEvent event)
	{
		if(Minecraft.getMinecraft().theWorld == null)
			return;
		
		breadcrumbUtils.updateTick();
	}
	
	@SubscribeEvent
	public void onRenderWorldLastEvent(RenderWorldLastEvent event)
	{
		breadcrumbUtils.renderBreadcrumbs(event.partialTicks);
	}
	
}
