package ooo.marto.martobreadcrumbs;

import net.minecraft.init.Blocks;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import ooo.marto.martobreadcrumbs.commands.BreadcrumbsCommand;
import ooo.marto.martobreadcrumbs.listener.BreadcrumbsListener;
import ooo.marto.martobreadcrumbs.utils.BreadcrumbsUtils;

@Mod(modid = "martobreadcrumbs", name = "Marto's Breadcrumbs", version = "1.0")
public class Breadcrumbs
{
   
	private BreadcrumbsUtils breadcrumbsUtils;
	
	private Configuration config;
	
	private static Breadcrumbs modInstance;
	
	public static Breadcrumbs getModInstance()
	{
		return modInstance;
	}
	
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		config = new Configuration(event.getSuggestedConfigurationFile());
		
		try
		{
			config.load();
			
			BreadcrumbsSettings.TNT_ENABLED = config.get(Configuration.CATEGORY_GENERAL, "tntEnabled", true).getBoolean();
			BreadcrumbsSettings.SAND_ENABLED = config.get(Configuration.CATEGORY_GENERAL, "sandEnabled", true).getBoolean();
			
			BreadcrumbsSettings.THICKNESS = (float) config.get(Configuration.CATEGORY_GENERAL, "thickness", 3.0).getDouble();
			
			BreadcrumbsSettings.TNT_RED = (float) config.get(Configuration.CATEGORY_GENERAL, "tntRed", 1.0).getDouble();
			BreadcrumbsSettings.TNT_GREEN = (float) config.get(Configuration.CATEGORY_GENERAL, "tntGreen", 0.0).getDouble();
			BreadcrumbsSettings.TNT_BLUE = (float) config.get(Configuration.CATEGORY_GENERAL, "tntBlue", 0.0).getDouble();
			
			BreadcrumbsSettings.SAND_RED = (float) config.get(Configuration.CATEGORY_GENERAL, "sandRed", 0.2).getDouble();
			BreadcrumbsSettings.SAND_GREEN = (float) config.get(Configuration.CATEGORY_GENERAL, "sandGreen", 0.6).getDouble();
			BreadcrumbsSettings.SAND_BLUE = (float) config.get(Configuration.CATEGORY_GENERAL, "sandBlue", 1.0).getDouble();
			
			BreadcrumbsSettings.TIME_TO_LIVE = (float) config.get(Configuration.CATEGORY_GENERAL, "timeToLive", 20.0).getDouble();
		} catch(Exception e)
		{
			
		} finally
		{
			if (config.hasChanged()) config.save();
		}
	}
	
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
    	modInstance = this;
    	breadcrumbsUtils = new BreadcrumbsUtils();
    	
		MinecraftForge.EVENT_BUS.register(new BreadcrumbsListener());
		ClientCommandHandler.instance.registerCommand(new BreadcrumbsCommand());
    }
    
    public BreadcrumbsUtils getBreadcrumbsUtils()
    {
    	return breadcrumbsUtils;
    }
    
    public Configuration getConfiguration()
    {
    	return config;
    }
    
}
