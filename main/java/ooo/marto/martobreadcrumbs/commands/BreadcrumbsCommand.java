package ooo.marto.martobreadcrumbs.commands;

import java.util.Arrays;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.common.config.Configuration;
import ooo.marto.martobreadcrumbs.Breadcrumbs;
import ooo.marto.martobreadcrumbs.BreadcrumbsSettings;

public class BreadcrumbsCommand implements ICommand {

	private Configuration config;
	
	public BreadcrumbsCommand()
	{
		config = Breadcrumbs.getModInstance().getConfiguration();
	}
	
	@Override
	public int compareTo(ICommand arg0) {
		return 0;
	}

	@Override
	public String getCommandName() {
		return "breadcrumbs";
	}

	@Override
	public String getCommandUsage(ICommandSender sender) {
		return "breadcrumbs";
	}

	@Override
	public List<String> getCommandAliases() {
		return Arrays.asList("");
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args) throws CommandException {
		if (args.length == 0) {
			displayHelpMenu();
			return;
		}

		if (args.length == 1) {
			if (args[0].equalsIgnoreCase("tnt")) {
				BreadcrumbsSettings.TNT_ENABLED = !BreadcrumbsSettings.TNT_ENABLED;
				sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED.toString() + "Breadcrumbs > "
						+ EnumChatFormatting.GRAY.toString() + "TNT Breadcrumbs are now "
						+ (BreadcrumbsSettings.TNT_ENABLED ? EnumChatFormatting.GREEN.toString() + "on"
								: EnumChatFormatting.RED.toString() + "off")
						+ EnumChatFormatting.GRAY.toString() + "."));
				try
				{
				config.get(Configuration.CATEGORY_GENERAL, "tntEnabled", true).set(BreadcrumbsSettings.TNT_ENABLED);
				Breadcrumbs.getModInstance().getConfiguration().save();
				} catch(Exception e) {}
				return;
			}

			if (args[0].equalsIgnoreCase("sand")) {
				BreadcrumbsSettings.SAND_ENABLED = !BreadcrumbsSettings.SAND_ENABLED;
				sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED.toString() + "Breadcrumbs > "
						+ EnumChatFormatting.GRAY.toString() + "Sand Breadcrumbs are now "
						+ (BreadcrumbsSettings.SAND_ENABLED ? EnumChatFormatting.GREEN.toString() + "on"
								: EnumChatFormatting.RED.toString() + "off")
						+ EnumChatFormatting.GRAY.toString() + "."));
				try
				{
				config.get(Configuration.CATEGORY_GENERAL, "sandEnabled", true).set(BreadcrumbsSettings.SAND_ENABLED);
				Breadcrumbs.getModInstance().getConfiguration().save();
				} catch(Exception e) {}
				return;
			}

			displayHelpMenu();
			return;
		}

		if (args.length == 2) {
			if (args[0].equalsIgnoreCase("thickness")) {
				try {
					BreadcrumbsSettings.THICKNESS = Float.parseFloat(args[1]);
					sender.addChatMessage(new ChatComponentText(
							EnumChatFormatting.RED.toString() + "Breadcrumbs > " + EnumChatFormatting.GRAY.toString()
									+ "Breadcrumbs thickness set to " + EnumChatFormatting.LIGHT_PURPLE.toString()
									+ args[1] + EnumChatFormatting.GRAY.toString() + "."));
					try {
						config.get(Configuration.CATEGORY_GENERAL, "thickness", 3).setValue(BreadcrumbsSettings.THICKNESS);
						config.save();
					} catch(Exception e) {}
				} catch (Exception e) {
					sender.addChatMessage(new ChatComponentText(
							EnumChatFormatting.RED.toString() + "Breadcrumbs > Invalid argument! Expected a number."));
				}
				return;
			}

			if (args[0].equalsIgnoreCase("time")) {
				try {
					float timeToLive = Float.parseFloat(args[1]);

					if (timeToLive < 0) {
						sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED.toString()
								+ "Breadcrumbs > Invalid argument! Number must be positive."));
						return;
					}

					BreadcrumbsSettings.TIME_TO_LIVE = timeToLive;
					sender.addChatMessage(new ChatComponentText(
							EnumChatFormatting.RED.toString() + "Breadcrumbs > " + EnumChatFormatting.GRAY.toString()
									+ "Breadcrumbs duration set to " + EnumChatFormatting.LIGHT_PURPLE.toString()
									+ args[1] + EnumChatFormatting.GRAY.toString() + "."));
					try {
						config.get(Configuration.CATEGORY_GENERAL, "timeToLive", 3).setValue(BreadcrumbsSettings.TIME_TO_LIVE);
						config.save();
					} catch(Exception e) {}
				} catch (Exception e) {
					sender.addChatMessage(new ChatComponentText(
							EnumChatFormatting.RED.toString() + "Breadcrumbs > Invalid argument! Expected a number."));
				}
				return;
			}

			displayHelpMenu();
			return;
		}

		if (args.length == 4) {
			if (args[0].equalsIgnoreCase("tntcolor")) {
				try {
					float red = Float.parseFloat(args[1]) / 255, green = Float.parseFloat(args[2]) / 255,
							blue = Float.parseFloat(args[3]) / 255;

					if (red < 0 || red > 1 || green < 0 || green > 1 || blue < 0 || blue > 1) {
						sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED.toString()
								+ "Breadcrumbs > Invalid argument! Number must be in range of 0-255."));
						return;
					}

					BreadcrumbsSettings.TNT_RED = red;
					BreadcrumbsSettings.TNT_GREEN = green;
					BreadcrumbsSettings.TNT_BLUE = blue;

					sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED.toString() + "Breadcrumbs > "
							+ EnumChatFormatting.GRAY.toString() + "TNT Breadcrumbs color updated."));
					try {
						config.get(Configuration.CATEGORY_GENERAL, "tntRed", 3).setValue(BreadcrumbsSettings.TNT_RED);
						config.get(Configuration.CATEGORY_GENERAL, "tntGreen", 3).setValue(BreadcrumbsSettings.TNT_GREEN);
						config.get(Configuration.CATEGORY_GENERAL, "tntBlue", 3).setValue(BreadcrumbsSettings.TNT_BLUE);
						config.save();
					} catch(Exception e) {}
				} catch (Exception e) {
					sender.addChatMessage(new ChatComponentText(
							EnumChatFormatting.RED.toString() + "Breadcrumbs > Invalid argument! Expected a number."));
				}
				return;
			}

			if (args[0].equalsIgnoreCase("sandcolor")) {
				try {
					float red = Float.parseFloat(args[1]) / 255, green = Float.parseFloat(args[2]) / 255,
							blue = Float.parseFloat(args[3]) / 255;

					BreadcrumbsSettings.SAND_RED = red;
					BreadcrumbsSettings.SAND_GREEN = green;
					BreadcrumbsSettings.SAND_BLUE = blue;

					sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED.toString() + "Breadcrumbs > "
							+ EnumChatFormatting.GRAY.toString() + "Sand Breadcrumbs color updated."));
					try {
						config.get(Configuration.CATEGORY_GENERAL, "sandRed", 3).setValue(BreadcrumbsSettings.SAND_RED);
						config.get(Configuration.CATEGORY_GENERAL, "sandGreen", 3).setValue(BreadcrumbsSettings.SAND_GREEN);
						config.get(Configuration.CATEGORY_GENERAL, "sandBlue", 3).setValue(BreadcrumbsSettings.SAND_BLUE);
						config.save();
					} catch(Exception e) {}
				} catch (Exception e) {
					sender.addChatMessage(new ChatComponentText(
							EnumChatFormatting.RED.toString() + "Breadcrumbs > Invalid argument! Expected a number."));
				}
				return;
			}

		}
		displayHelpMenu();
	}

	private void displayHelpMenu() {
		EntityPlayerSP player = Minecraft.getMinecraft().thePlayer;
		
		player.addChatMessage(
				new ChatComponentText(EnumChatFormatting.GRAY.toString() + "--------------- " + EnumChatFormatting.RED + "Marto's Breadcrumbs Mod" + EnumChatFormatting.GRAY + " ---------------"));
		player.addChatMessage(new ChatComponentText(
				EnumChatFormatting.RED.toString() + "/breadcrumbs tnt" + EnumChatFormatting.GRAY.toString() + " - "
						+ EnumChatFormatting.RED.toString() + "Toggles TNT breadcrumbs."));
		player.addChatMessage(new ChatComponentText(
				EnumChatFormatting.RED.toString() + "/breadcrumbs sand" + EnumChatFormatting.GRAY.toString() + " - "
						+ EnumChatFormatting.RED.toString() + "Toggles Sand breadcrumbs."));
		player.addChatMessage(new ChatComponentText(
				EnumChatFormatting.RED.toString() + "/breadcrumbs time <seconds>" + EnumChatFormatting.GRAY.toString()
						+ " - " + EnumChatFormatting.RED.toString() + "Sets duration of breadcrumbs."));
		player.addChatMessage(new ChatComponentText(EnumChatFormatting.RED.toString()
				+ "/breadcrumbs thickness <thickness>" + EnumChatFormatting.GRAY.toString() + " - "
				+ EnumChatFormatting.RED.toString() + "Changes thickness."));
		player.addChatMessage(new ChatComponentText(EnumChatFormatting.RED.toString()
				+ "/breadcrumbs tntcolor <red> <green> <blue>" + EnumChatFormatting.GRAY.toString() + " - "
				+ EnumChatFormatting.RED.toString() + "TNT color."));
		player.addChatMessage(new ChatComponentText(EnumChatFormatting.RED.toString()
				+ "/breadcrumbs sandcolor <red> <green> <blue>" + EnumChatFormatting.GRAY.toString() + " - "
				+ EnumChatFormatting.RED.toString() + "Sand color."));

	}

	@Override
	public boolean canCommandSenderUseCommand(ICommandSender sender) {
		return true;
	}

	@Override
	public List<String> addTabCompletionOptions(ICommandSender sender, String[] args, BlockPos pos) {
		return Arrays.asList("");
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index) {
		return false;
	}

}
